# Specify the base image
FROM python:3.10

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file to the working directory
COPY src/requirements.txt .
#COPY src/main.py /app/main.py
COPY . /app

# Install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy the application code to the working directory
# COPY . .
COPY src/ .

CMD ["python", "src/main.py"]
# ENTRYPOINT ["python", "main.py"]

