#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is the quality check for CSV files.
#

import os, csv, re
import configparser
from math import inf
import pandas as pd
import numpy as np
from fpdf import FPDF

#
# FORMAT SPECIFICATION OPTIONS:
#
# + FOR ALL COLUMNS:
# + + type = integer|float|bool|date|datetime|string
# + + required = True|False|Yes|No
#
# + FOR NUMERIC COLUMNS:
# + + minval = <integer>|<float>
# + + maxval = <integer>|<float>
#
# + FOR STRING COLUMNS:
# + + minlen = <integer>
# + + maxlen = <integer>
# + + pattern = <regular expression>
#

# Global variables used to store the results.
LOG = ""
HTM = ""
FMT = {}

# Common date formats used for parsing.
date_fmts = ("%x", "%c", "%x %X", "%Y",
             "%m/%d/%Y", "%m/%d/%y", "%Y-%m-%d", "%Y/%m/%d",
             "%b %d, %Y", "%b %d %Y", "%d %b, %Y", "%d %b %Y",
             "%b. %d, %Y", "%b. %d %Y", "%d %b., %Y", "%d %b. %Y",
             "%b %Y", "%b, %Y", "%b. %Y", "%b., %Y", "%b-%Y", "%b.-%Y",
             "%B %d, %Y", "%B %d %Y", "%d %B, %Y", "%d %B %Y",
             "%B %Y", "%B, %Y", "%B-%Y")

# Common date and time formats used for parsing.
datetime_fmts = ("%x", "%c", "%x %X", "%Y",
                 "%m/%d/%Y", "%m/%d/%Y %H%M", "%m/%d/%Y %I:%M %p",
                 "%m/%d/%y", "%m/%d/%y %H%M", "%m/%d/%y %I:%M %p",
                 "%Y-%m-%d", "%Y-%m-%d %H%M", "%Y-%m-%d %I:%M %p",
                 "%Y/%m/%d", "%Y/%m/%d %H%M", "%Y/%m/%d %I:%M %p",
                 "%b %d, %Y", "%b %d, %Y %X", "%b %d, %Y %I:%M %p",
                 "%b %d %Y", "%b %d %Y %X", "%b %d %Y %I:%M %p",
                 "%d %b, %Y", "%d %b, %Y %X", "%d %b, %Y %I:%M %p",
                 "%d %b %Y", "%d %b %Y %X", "%d %b %Y %I:%M %p",
                 "%b. %d, %Y", "%b. %d, %Y %X", "%b. %d, %Y %I:%M %p",
                 "%b. %d %Y", "%b. %d %Y %X", "%b. %d %Y %I:%M %p",
                 "%d %b., %Y", "%d %b., %Y %X", "%d %b., %Y %I:%M %p",
                 "%d %b. %Y", "%d %b. %Y %X", "%d %b. %Y %I:%M %p",
                 "%b %Y", "%b, %Y", "%b. %Y", "%b., %Y", "%b-%Y", "%b.-%Y",
                 "%B %d, %Y", "%B %d, %Y %X", "%B %d, %Y %I:%M %p",
                 "%B %d %Y", "%B %d %Y %X", "%B %d %Y %I:%M %p",
                 "%d %B, %Y", "%d %B, %Y %X", "%d %B, %Y %I:%M %p",
                 "%d %B %Y", "%d %B %Y %X", "%d %B %Y %I:%M %p",
                 "%B %Y", "%B, %Y", "%B-%Y")


# Checks the validity of integer values.
# data: table cell entry
# minval: lowest possible value
# maxval: highest possible value
# required: cell must not be empty
# return: status, error message
def check_int(data, minval=-inf, maxval=inf, required=False):
    if not data:
        if not required:
            return 2, ""  # EMPTY/UNDEFINED
        else:
            return 0, "missing entry"  # ERROR

    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry"  # WARNING

    else:
        try:
            value = int(data)
            if value < minval:
                return 1, "value too small"  # WARNING
            elif value > maxval:
                return 1, "value too large"  # WARNING
            else:
                return 3, ""  # SUCCESS
        except ValueError:
            return 0, "not an integer"  # ERROR


# Checks the validity of float values.
# data: table cell entry
# minval: lowest possible value
# maxval: highest possible value
# required: cell must not be empty
# return: status, error message
def check_float(data, minval=-inf, maxval=inf, required=False):
    if not data:
        if not required:
            return 2, ""  # EMPTY/UNDEFINED
        else:
            return 0, "missing entry"  # ERROR

    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry",  # WARNING

    else:
        try:
            value = float(data)
            if value < minval:
                return 1, "value too small"  # WARNING
            elif value > maxval:
                return 1, "value too large"  # WARNING
            else:
                return 3, ""  # SUCCESS
        except ValueError:
            return 0, "not a float"  # ERROR


# Checks the validity of boolean values.
# data: table cell entry
# required: cell must not be empty
# return: status, error message
def check_bool(data, required=False):
    if not data:
        if not required:
            return 2, ""  # EMPTY/UNDEFINED
        else:
            return 0, "missing entry"  # ERROR

    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry",  # WARNING

    elif data in ('True', 'true', 'TRUE', 'Yes', 'yes', 'YES', True):
        return 3, ""  # SUCCESS
    elif data in ('False', 'false', 'FALSE', 'No', 'no', 'NO', False):
        return 3, ""  # SUCCESS
    else:
        return 0, "not a boolean"  # ERROR


# Checks the validity of string values.
# data: table cell entry
# minlen: minimum string length
# maxlen: maximum string length
# pattern: regular expression
# required: cell must not be empty
# return: status, error message
def check_string(data, minlen=0, maxlen=inf, pattern=None, required=False):
    if not data:
        if not required:
            return 2, ""  # EMPTY/UNDEFINED
        else:
            return 0, "missing entry"  # ERROR

    elif len(data) < minlen:
        return 1, "data too short"  # WARNING
    elif len(data) > maxlen:
        return 1, "data too long"  # WARNING

    elif pattern:
        if pattern.match(data):
            return 3, ""  # SUCCESS
        else:
            return 1, "pattern mismatch"  # WARNING
    else:
        return 3, ""  # SUCCESS


# Validates the table format specifications.
# col: table column name
# format: format specifications
# return: True if there are no errors/warnings
def validate_header(col, format):
    global LOG, HTM, FMT
    log = ""
    chk = 3

    type = "string"
    if "type" in format:
        if format["type"] in ("integer", "float", "bool", "string"):
            type = format["type"]
        else:
            log += '\n [WRN] in header, column "' + col + '": unknown type "' + format["type"] + '"'
            chk = min(chk, 1)

    required = False
    if "required" in format:
        if format["required"] in ("True", "true", "TRUE", "Yes", "yes", "YES"):
            required = True
        elif format["required"] not in ("False", "false", "FALSE", "No", "no", "NO"):
            log += '\n [ERR] in header, column "' + col + '": invalid value for required'
            chk = min(chk, 0)

    minval = -inf
    if "minval" in format:
        if type in ("integer", "float"):
            try:
                minval = float(format["minval"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for minval'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": minval for non-numeric type'
            chk = min(chk, 1)

    maxval = inf
    if "maxval" in format:
        if type in ("integer", "float"):
            try:
                maxval = float(format["maxval"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for maxval'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": maxval for non-numeric type'
            chk = min(chk, 1)

    minlen = 0
    if "minlen" in format:
        if type in ("string"):
            try:
                minlen = int(format["minlen"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for minlen'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": minlen for non string-type'
            chk = min(chk, 1)

    maxlen = inf
    if "maxlen" in format:
        if type in ("string"):
            try:
                maxlen = int(format["maxlen"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for maxlen'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": maxlen for non string-type'
            chk = min(chk, 1)

    pattern = None
    if "pattern" in format:
        if type in ("string"):
            try:
                pattern = re.compile(format["pattern"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid regular expression'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": pattern for non string-type'
            chk = min(chk, 1)

    for key in format.keys():
        if key not in ("type", "required", "minval", "maxval", "minlen", "maxlen", "pattern"):
            log += '\n [WRN] in header, column "' + col + '": unknown argument "' + key + '"'
            chk = min(chk, 1)

    FMT[col] = {"type": type, "required": required, "minval": minval, "maxval": maxval,
                "minlen": minlen, "maxlen": maxlen, "pattern": pattern}

    if chk == 0:  # ERROR / RED
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#800000;"><code>' + col + '</code></span></th>\n'
    elif chk == 1:  # WARNING / YELLOW
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#705000;"><code>' + col + '</code></span></th>\n'
    elif chk == 2:  # UNDEFINED / WHITE
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#202020"><code>' + col + '</code></span></th>\n'
    elif chk == 3:  # SUCCESS / GREEN
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#004000;"><code>' + col + '</code></span></th>\n'

    LOG += log
    return log == ""


# Validates the table format specifications.
# data: table cell entry
# row: table row number
# col: table column name
# return: True if there are no errors/warnings
def validate_cell(data, row, col):
    global LOG, HTM, FMT
    log = ""

    if FMT[col]["type"] == "integer":
        chk, desc = check_int(data, FMT[col]["minval"], FMT[col]["maxval"], FMT[col]["required"])
    elif FMT[col]["type"] == "float":
        chk, desc = check_float(data, FMT[col]["minval"], FMT[col]["maxval"], FMT[col]["required"])
    elif FMT[col]["type"] == "bool":
        chk, desc = check_bool(data, FMT[col]["required"])
    elif FMT[col]["type"] == "string":
        chk, desc = check_string(data, FMT[col]["minlen"], FMT[col]["maxlen"], FMT[col]["pattern"],
                                 FMT[col]["required"])

    if chk == 0:
        log += '\n [ERR] in row ' + str(row) + ', column "' + col + '": ' + desc
    elif chk == 1:
        log += '\n [WRN] in row ' + str(row) + ', column "' + col + '": ' + desc

    if chk == 0:  # ERROR / RED
        HTM += '\t\t\t<td title="' + desc + '" bgcolor=#FFBFBF><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 1:  # WARNING / YELLOW
        HTM += '\t\t\t<td title="' + desc + '" bgcolor=#FFFF7F><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 2:  # UNDEFINED / WHITE
        HTM += '\t\t\t<td bgcolor=#F7F7F7><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 3:  # SUCCESS / GREEN
        HTM += '\t\t\t<td bgcolor=#BFFFBF><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'

    LOG += log
    return log == ""


# Opens and validates the csv table.
# csv_path: path to the csv file
# fmt_path: path to the format file
# return: status, feedback
def validate_table(csv_path, fmt_path=""):
    global LOG, HTM, FMT
    LOG = ""
    HTM = ""
    FMT = {}

    config = {}
    status = 2
    feedback = "SUCCESS: The CSV file is well-formed. It matches the format declaration and therefore is valid."
    if not fmt_path:
        status = 1
        feedback = "WARNING: The CSV file is well-formed. Please provide a format file (.ini) to check for validity."

    else:
        try:
            parser = configparser.ConfigParser()
            parser.read_file(open(fmt_path, 'r'))
            for column, _ in parser.items():
                config[column] = {}
                for key, value in parser.items(column):
                    config[column][key] = value

        except IOError:
            status = 1
            feedback = "WARNING: The CSV file is well-formed. The format file could not be opened or is missing."
        except:
            status = 1
            feedback = "WARNING: The CSV file is well-formed. The format file could not be parsed or has errors."

    try:
        dialect = csv.Sniffer().sniff(open(csv_path, 'r').read(1024))  # get dialect for different delimiters
        table = csv.reader(open(csv_path, 'r'), dialect=dialect)

        line_nums = False
        start = 1
        for row in table:
            break
        for row in table:
            try:
                curr = int(row[0])
                if curr not in (0, 1, 2):
                    prev = -inf
                    break
                start = curr
                prev = curr
                break
            except:
                break
        for row in table:
            try:
                curr = int(row[0])
                if curr != prev + 1:
                    break
                prev = curr
            except:
                break
        else:
            line_nums = True

        table = csv.reader(open(csv_path, 'r'), dialect=dialect)

        HTM += '<html><meta charset="UTF-8">\n'
        HTM += '\t<table border="0" class="dataframe">\n'
        HTM += '\t<thead>\n'

        for row in table:
            header = row
            line = start - 1
            HTM += '\t\t<tr style="text-align:center;vertical-align:middle;">\n'
            if line < 1:
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#F7F7F7><span style="font-size:15px;"><code></code></span></th>\n'
            else:
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;"><code>' + str(
                    line) + '</code></span></th>\n'
            for i in range(len(row)):
                if line_nums and i == 0:
                    continue
                if not row[i] in config:
                    config[row[i]] = {}
                    if status > 1:
                        status = 1
                        feedback = "WARNING: The CSV file is well-formed. The format declaration for some columns is missing."
                if not validate_header(row[i], config[row[i]]) and status > 1:
                    status = 1
                    feedback = "WARNING: The CSV file is well-formed. The format declaration has some errors or warnings."
            HTM += '\t\t</tr>\n'
            break

        HTM += '\t</thead>\n'
        HTM += '\t<tbody>\n'

        for row in table:
            line += 1
            HTM += '\t\t<tr style="text-align:center;vertical-align:middle;">\n'
            HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;"><code>' + str(
                line) + '</code></span></th>\n'
            if len(row) != len(header):
                status = 0
                feedback = "ERROR: The CSV file is not well-formed. The rows differ in length or columns are missing."
                break
            for i in range(len(row)):
                if line_nums and i == 0:
                    continue
                if not validate_cell(row[i], line, header[i]) and status > 1:
                    status = 1
                    feedback = "WARNING: The CSV file does not match the format declaration. It is well-formed, but not valid."
            HTM += '\t\t</tr>\n'

        HTM += '\t</tbody>\n'
        HTM += '\t</table>\n'
        HTM += '</html>'

    except IOError:
        status = 0
        feedback = "ERROR: The CSV file could not be opened. Please check if the file is missing or corrupted."
    except:
        status = 0
        feedback = "ERROR: The CSV file could not be parsed. It is not well-formed and contains syntax errors."

    return status, feedback


def list_to_str(list_info):
    """convert list to string"""
    return '\n'.join([str(element) for element in list_info])


def duplicate_column_check(data, name):
    """Duplicate column check"""
    column_unique = []  # unique column check (two columns are same or not)
    for col in data.columns:
        for col_2 in data.columns:
            if col == col_2:
                continue
            else:
                column_temp = data[col].equals(data[col_2])
                if column_temp:
                    column_check = "Column name: " + col_2
                    column_unique.append(column_check)
    return column_unique


def row_unique_check(data, name):
    """Duplicate Row check"""
    row_unique = []
    for row_one in range(0, data.shape[0]):
        for row_all in range(0, data.shape[0]):
            if row_one == row_all:
                continue
            else:
                data1 = data.values[row_one].tolist()
                x_data = [element for element in data1 if str(element) != "nan"]
                x_data = np.array(x_data)
                data2 = data.values[row_all].tolist()
                y_data = [element for element in data2 if str(element) != "nan"]
                y_data = np.array(y_data)
                row_equal = np.array_equal(x_data, y_data)
                if row_equal:
                    row_check = "Row number: " + str(row_all + 1)
                    row_unique.append(row_check)
    return row_unique


def column_empty_check(data, name):
    """Full empty column check"""
    column_empty = []  # empty column check
    col_number = 1
    for col in data.columns:
        c_series = pd.isnull(data[col])
        c_list = c_series.tolist()
        c_list_len = len(c_list)
        c_check_nan = [element for element in c_list if str(element) == 'True']
        c_check_nan_len = len(c_check_nan)
        if c_check_nan_len == c_list_len:
            c_empty = "Column number " + str(col_number) + " column name: " + col + " are full empty"
            column_empty.append(c_empty)
        col_number = col_number + 1
    return column_empty


def row_empty_check(data, name):
    """Full empty row check"""
    row_empty = []
    row_number = 1
    for row_one in range(0, data.shape[0]):
        r_list = data.values[row_one].tolist()
        r_list_len = len(r_list)
        r_check = [element for element in r_list if str(element) == "nan"]
        r_check_len = len(r_check)
        if r_check_len == r_list_len:
            r_empty = "Row number " + str(row_number) + " are full empty."
            row_empty.append(r_empty)
        row_number = row_number + 1
    return row_empty


def column_same_value_check(data):
    """All column values are same"""
    column_same_value = []  # all the column values are same or not (particular one column)
    for col in data.columns:
        col_array = data[col].unique()
        col_list = col_array.tolist()
        if len(col_list) <= 1:
            col_list_new = [x for x in col_list if pd.isnull(x) == False and x != 'nan']
            if len(col_list_new) == 1:
                column_same = "Column name: " + col + " all values are same."
                column_same_value.append(column_same)
    return column_same_value


def missing_cell_count_check(data, cells_count):
    """Empty value check"""
    non_value_check = []
    cols_new = data.columns
    for i in cols_new.values:
        row_temp = 1
        for j in data[i]:
            if pd.isna(j):
                non_value = "Nan Values found in row: " + str(row_temp + 1) + " in column: " + str(i)
                non_value_check.append(non_value)
                cells_count = cells_count + 1
            row_temp = row_temp + 1
    return non_value_check, cells_count


def column_nan_zero_value_count(data):
    """Column wise nan values and zero values count"""
    column_nan_value_count = []
    column_zero_value_count = []
    cols_new = data.columns

    # Column wise missing values count
    for col in cols_new:
        if data[col].isna().sum() != 0:
            column_nan_value = "Column name: " + col + "\t missing values: " + str(data[col].isna().sum())
            column_nan_value_count.append(column_nan_value)

    # Column wise zero values count
    for col in cols_new:
        if (data[col] == 0).sum() != 0:
            column_zero_value = "Column name: " + col + "\t zero values: " + str((data[col] == 0).sum())
            column_zero_value_count.append(column_zero_value)
    return column_nan_value_count, column_zero_value_count


# Creates dictionary of feedback files and badge status.
# paths_dict: path of file which is checked
# path: root path of the repository
# log_path: path where feedback is stored on the server
# return: dictionary of feedback files and badge status
def create_dict_values(paths_dict, path, log_path):

    # Create the 'public' folder if it doesn't exist
    if not os.path.exists('public'):
        os.makedirs('public')

    global LOG, HTM, FMT
    files_dict = {}
    log_path_list = []
    for csv_name, fmt in paths_dict.items():
        feedback = ""
        if not fmt:
            files_dict[csv_name], feedback = validate_table(csv_name)
        else:
            files_dict[csv_name], feedback = validate_table(csv_name, fmt)
        # val = csv_name.rsplit(path, 1)[1].strip("/")
        # output_path = os.path.join(log_path, val)
        # os.makedirs(output_path.rsplit("/", 1)[0], exist_ok=True)
        file_name = str(csv_name).rsplit("/")[-1].rsplit(".")[0]
        print(file_name)
        log_path_new = log_path + "/" + file_name
        print(log_path_new)
        data = pd.read_csv(csv_name)
        row_count = "Row Number: " + str(len(data)) + "\n"
        col_count = "Column Number: " + str(len(data.columns))
        general_information = "\n\nGeneral Information:\n" + row_count + col_count

        # Duplicate column check
        column_unique = duplicate_column_check(data, file_name)
        if column_unique:
            column_unique_str = "\n\n[WAR] The following columns are duplicate (Pair):\n" + list_to_str(column_unique)
        else:
            column_unique_str = "\n\n[Good] No columns are duplicate (Pair)."

        # Duplicate Row, Row count and percentage
        row_unique_count = []
        row_unique_count_percentage = []
        if data.duplicated().sum():
            row_count_duplicate = "\n\n[WAR] Duplicate row count: " + str(data.duplicated().sum())
            row_unique_count.append(row_count_duplicate)
            row_percentage = "\n\n[WAR] Percentage of duplicate row : " + str(
                round((data.duplicated().sum() / len(data)) * 100, 2)) + "%"
            row_unique_count_percentage.append(row_percentage)
        else:
            row_count_duplicate = "\n\n[Good] No duplicate row found."
            row_unique_count.append(row_count_duplicate)
            row_percentage = "\n\n[Good] 0% duplicate row."
            row_unique_count_percentage.append(row_percentage)

        row_unique = row_unique_check(data, file_name)
        if row_unique:
            row_unique_str = "\n\n[WAR] The following rows are same: \n" + list_to_str(np.unique(row_unique))
        else:
            row_unique_str = "\n\n[Good] No rows are same.\n"
        row_unique_count_str = list_to_str(np.unique(row_unique_count))
        row_unique_count_percentage_str = list_to_str(np.unique(row_unique_count_percentage))

        # Full empty row and column check
        column_empty = column_empty_check(data, file_name)
        if column_empty:
            column_empty_str = "\n\n[WAR] Empty Columns:  \n" + list_to_str(np.unique(column_empty))
        else:
            column_empty_str = "\n\n[Good] No empty columns."
        row_empty = row_empty_check(data, file_name)
        if row_empty:
            row_empty_str = "\n\n[WAR] Empty Rows: \n" + list_to_str(np.unique(row_empty))
        else:
            row_empty_str = "\n\n[Good] No empty rows."

        # All column values are same
        column_same_value = column_same_value_check(data)
        if column_same_value:
            column_same_value_str = "\n\n[WAR] The following are same column values (particular one column): \n" + list_to_str(
                np.unique(column_same_value))
        else:
            column_same_value_str = "\n\n[Good] No column values are same (particular one column)\n"

        # Empty value check
        missing_cell_info = []
        cells_count = 0
        total_cells = len(data.columns) * len(data)
        non_value_check, missing_cells_count = missing_cell_count_check(data, cells_count)
        missing_cell = "Total cells: " + str(total_cells) + " missing cells: " + str(
            missing_cells_count) + " missing cells percentage: " + str(
            round((missing_cells_count / total_cells) * 100, 2)) + "%"
        missing_cell_info.append(missing_cell)
        if missing_cell_info:
            missing_cell_info_str = "\n\n[WAR] Empty Cells information: \n" + list_to_str(missing_cell_info)
        else:
            missing_cell_info_str = "\n\n[Good] No missing cells found.\n"

        # Column wise nan values and zero values count
        column_nan_value_count, column_zero_value_count = column_nan_zero_value_count(data)
        if column_nan_value_count:
            column_nan_value_count_str = "\n\n[WAR] Nan Value Check: \n" + list_to_str(
                np.unique(column_nan_value_count))
        else:
            column_nan_value_count_str = "\n\n[Good] No Nan values found."
        if column_zero_value_count:
            column_zero_value_count_str = "\n\n[WAR] Zero Value Check: \n" + list_to_str(
                np.unique(column_zero_value_count))
        else:
            column_zero_value_count_str = "\n\n[Good] No Zero values found."

        temp_file_name = str(csv_name).rsplit("/")[-1].rsplit(".")[0]
        create_log(log_path_new + "_.log", temp_file_name + "_.log",
                   feedback + LOG + general_information + column_unique_str + row_unique_str + row_unique_count_str
                   + row_unique_count_percentage_str + column_empty_str + row_empty_str + column_same_value_str
                   + missing_cell_info_str + column_nan_value_count_str + column_zero_value_count_str)
        create_log(log_path_new + "_.html", temp_file_name + "_.html", HTM)
        log_path_list.append(log_path_new)

    create_pdf(log_path_list)

    return files_dict


# Writes the results to a file.
# logfile: path of the logfile on the server
# content: feedback which is written into file
def create_log(logfile, logfile_new, content):
    if not os.path.exists('public'):
        os.makedirs('public')

    with open(logfile, 'w+') as log_file:
        log_file.write(str(content))

    with open("public/" + logfile_new, 'w+') as log_file:
        log_file.write(content)



title = 'Quality Check [CSV]'


class PDF_csv(FPDF):
    def header(self):
        if self.page_no() == 1:
            self.set_font('times', 'B', 25)
            title_w = self.get_string_width(title)
            doc_w = self.w
            self.set_x((doc_w - title_w) / 2)
            # self.set_fill_color(135, 206, 250)  # background
            self.set_text_color(0, 0, 0)  # text
            self.cell(title_w, 10, title, ln=1, align='C')  # fill=1
            self.ln(10)

    def footer(self):
        self.set_y(-15)
        self.set_font('times', 'B', 10)
        self.cell(0, 10, f'page {self.page_no()}', align='C')

    def chapter_body(self, txt, name):
        file_name = str(name).rsplit("/")[-1].rsplit("_")[0]
        self.set_left_margin(20)
        self.set_right_margin(50)
        self.set_top_margin(25)
        self.set_font('times', 'B', 16)
        self.cell(0, 20, 'File Name: ' + file_name + '.csv')
        self.ln(20)
        self.set_font('times', '', 12)
        self.multi_cell(0, 5, txt)
        # self.ln(5)
        # self.set_font('times', 'B', 16)
        # self.cell(0, 20, '[Source File]', link=name)
        self.ln(30)

    def source_file(self, name):
        self.set_font('times', 'B', 25)
        self.cell(0, 20, '[Source File]', link=name)


def create_pdf(logfiles):
    pdf_csv = PDF_csv('P', 'mm', 'A4')
    pdf_csv.set_auto_page_break(auto=True, margin=35)
    pdf_csv.add_page()
    # pdf.set_font('times', '', 16)
    # pdf.cell(10, 10, feedback, ln=True, border=True)
    for file in logfiles:
        with open(file + "_.log", 'r') as f:
            contents = f.read()
        pdf_csv.chapter_body(contents, file + "_.html")

    file_path = os.path.dirname(file)
    pdf_csv.output(file_path + '/output_csv.pdf')
    pdf_csv.output('public' + '/output_csv.pdf')
